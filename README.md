# Templates of Belgian invoices for non-profit organisations.

The modeles-factures-belges-associations extension is used to generate invoices for Belgian non-profit organisations.

## Crédits

- Author    : Robert Sebille, Rudy Cassart
- Maintainer: Robert Sebille, Rudy Cassart

- Licence   : Released under the LaTeX Project Public License v1.3c or later, see http://www.latex-project.org/lppl.txt
- % Depends of calctab, fancyhdr, ifthen, eurosym, hyperref, multirow, color,  colortbl

# Modèles de factures belges pour les associations

Cette extension XeLaTeX permet aux associations de générer des factures au modèle belge.

##Déni de toute responsabilité: 
personnellement, nous nous servons de ces factures et nous n'avons rencontré aucun problème à ce jour; mais nous ne ne sommes ni comptable, ni fiscaliste, et nous ne pouvons garantir que tout soit officiellement en ordre, nous partageons, c'est tout. 

C'est votre responsabilité de vérifier. Si vous avez des remarques ou des suggestions pertinentes à ce propos, vous pouvez nous les faire parvenir à partir du [bug tracker de la forge](https://gitlab.adullact.net/zenjo/modeles-fac­tures-belges-associations/issues).

## Crédits

- Author    : Robert Sebille, Rudy Cassart
- Maintainer: Robert Sebille, Rudy Cassart
- Licence   : Released under the LaTeX Project Public License v1.3c or later, see http://www.latex-project.org/lppl.txt
- % Depend de calctab, fancyhdr, ifthen, eurosym, hyperref, multirow, color,  colortbl

## Website
- [home](https://gitlab.adullact.net/zenjo/modeles-fac­tures-belges-associations/home) 
- [Annonces](https://gitlab.adullact.net/zenjo/modeles-fac­tures-belges-associations/wikis/Annonces)
- [Change log](https://gitlab.adullact.net/zenjo/modeles-fac­tures-belges-associations/wikis/Change-log)


