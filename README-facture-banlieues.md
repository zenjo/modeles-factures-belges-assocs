# Modèle article-banlieues
## Préambule
1. Voir la documentation générale;
1. ce modèle dépend du package caption: https://ctan.org/pkg/caption.

## Ce modèle permet essentiellement 
- l'ajout d'un logo personnalisé en haut à gauche;
- l'ajout d'un logo "sponsors" en bas de facture, surmonté d'une brève légende.

les codes \rsfbmentionObligatoire{} et \rsfbdateFacture{} permettent de récupérer et d'utiliser un numéro de facture un peu personnalisé et la date de la facture.